public class Application{
	public static void main(String[] args){
		Student s1 = new Student("Mike", 2336358, 21.3);
		Student s2 = new Student("Bob", 2235646, 34.8);
		
		System.out.println(s1.printName());
		System.out.println(s2.printName());
		s1.sayHi();
		s2.sayPizza();
		
		
		Student[] section3 = new Student[3];
		
		section3[0] = s1;
		section3[1] = s2;
		
		section3[2] = new Student("Mike", 2336358, 21.3);
		
		System.out.println(section3[1].printName());
		
		

	}
	
	
}